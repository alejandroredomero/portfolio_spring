package com.example.portfolio.controller;

import com.example.portfolio.model.*;
import com.example.portfolio.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class BasicController {
    @Autowired
    private DescripcionService descripcionService;
    @Autowired
    private LenguajesService lenguajesService;
    @Autowired
    private ProyectosService proyectosService;
    @Autowired
    private ContactoService contactoService;
    @Autowired
    private FooterService footerService;

    @GetMapping("/")
    String formulario (Model model){
        List<Descripcion>descripcions=descripcionService.getAllDescripcion();
        model.addAttribute("descripcion", descripcions);

        List<Lenguajes>lenguajes=lenguajesService.getAllLenguaje();
        model.addAttribute("lenguajes", lenguajes);

        List<Proyectos>proyectos=proyectosService.getAllProyecto();
        model.addAttribute("proyectos", proyectos);

        List<Footer>footer=footerService.getAllFooter();
        model.addAttribute("footer", footer);

        return "Portfolio";
    }

    @PostMapping("/")
    String add( @RequestParam String nombre, @RequestParam String apellidos, @RequestParam String email, @RequestParam String telefono, @RequestParam String mensaje, Model model){
        Contacto c = new Contacto();
        c.setNombre(nombre);
        c.setApellidos(apellidos);
        c.setEmail(email);
        c.setTelefono(telefono);
        c.setMensaje(mensaje);

        contactoService.addContacto(c);
        model.addAttribute("message", "Ya hemos agregado tu contacto.");


        List<Descripcion>descripcions=descripcionService.getAllDescripcion();
        model.addAttribute("descripcion", descripcions);

        List<Lenguajes>lenguajes=lenguajesService.getAllLenguaje();
        model.addAttribute("lenguajes", lenguajes);

        List<Proyectos>proyectos=proyectosService.getAllProyecto();
        model.addAttribute("proyectos", proyectos);

        List<Footer>footer=footerService.getAllFooter();
        model.addAttribute("footer", footer);

        return "Portfolio";
    }
}
