package com.example.portfolio.service;

import com.example.portfolio.model.Descripcion;
import com.example.portfolio.model.Proyectos;
import com.example.portfolio.repository.DescripcionRepository;
import com.example.portfolio.repository.ProyectosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProyectosService {
    @Autowired
    private ProyectosRepository proyectosRepository;
    public List<Proyectos> getAllProyecto(){
        return proyectosRepository.findAll();
    }

}
