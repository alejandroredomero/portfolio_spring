package com.example.portfolio.service;

import com.example.portfolio.model.Footer;
import com.example.portfolio.repository.FooterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FooterService {
    @Autowired
    private FooterRepository footerRepository;
    public List<Footer> getAllFooter(){
        return footerRepository.findAll();
    }
}