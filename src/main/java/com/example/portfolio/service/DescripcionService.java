package com.example.portfolio.service;

import com.example.portfolio.model.Descripcion;
import com.example.portfolio.repository.DescripcionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DescripcionService {
    @Autowired
    private DescripcionRepository movieRepository;
    public List<Descripcion> getAllDescripcion(){
        return movieRepository.findAll();
    }
}

