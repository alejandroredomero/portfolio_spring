package com.example.portfolio.service;

import com.example.portfolio.model.Contacto;
import com.example.portfolio.model.Descripcion;
import com.example.portfolio.repository.ContactoRepository;
import com.example.portfolio.repository.DescripcionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ContactoService {
    @Autowired
    private ContactoRepository contactoRepository;
    public Contacto addContacto(Contacto contacto){
        return contactoRepository.save(contacto);
    }

}
