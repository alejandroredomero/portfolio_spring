package com.example.portfolio.model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "proyectos")
public class Proyectos {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre_proyecto;
    private String foto_proyecto;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "relacion",
            joinColumns = @JoinColumn(name = "idproyectos"),
            inverseJoinColumns = @JoinColumn(name = "idlenguajes")
    )
    private List<Lenguajes> lenguajes;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre_proyecto() {
        return nombre_proyecto;
    }

    public void setNombre_proyecto(String nombre_proyecto) {
        this.nombre_proyecto = nombre_proyecto;
    }

    public String getFoto_proyecto() {
        return foto_proyecto;
    }

    public void setFoto_proyecto(String foto_proyecto) {
        this.foto_proyecto = foto_proyecto;
    }
}
