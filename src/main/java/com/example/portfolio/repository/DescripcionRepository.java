package com.example.portfolio.repository;

import com.example.portfolio.model.Descripcion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DescripcionRepository extends JpaRepository <Descripcion, Long> {
}
