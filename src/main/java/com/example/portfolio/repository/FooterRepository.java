package com.example.portfolio.repository;

import com.example.portfolio.model.Footer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FooterRepository extends JpaRepository<Footer, Long> {
}
