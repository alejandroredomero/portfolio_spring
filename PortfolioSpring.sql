CREATE DATABASE if not exists portfolio;
USE portfolio;

create table descripcion (
id int primary key,
logo varchar(255),
foto varchar(255),
nombre varchar(255),
texto varchar(255)
);

insert into descripcion values (
"1", "./img/augmented-reality_327785.png", "./img/MicrosoftTeams-image (2) - copia.png", "Alejandro Redomero Diaz", "Hola, soy Alejandro Redomero Díaz, soy estudiante, estudio un Grado Superior de DAM. Competencias que tengo pueden ser resolutivo, trabajo individual y grupal, adaptativo y sociable."

);

create table proyectos (
id int primary key,
nombre_proyecto varchar(255),
foto_proyecto varchar(255)
);

insert into proyectos values (
"1", "proyecto1", "./img/Roma - copia.png"
);
insert into proyectos values(
"2", "proyecto2", "./img/Sobre nosotros - copia.png"
);
insert into proyectos values(
"3", "proyecto3", "./img/tarea - copia.png"
);

 
create table lenguajes (
id int primary key,
nombre varchar(255),
foto varchar(255)
);

insert into lenguajes values (
"1", "Word", "./img/logoword.jpg"
);
insert into lenguajes values(
"2", "Java", "./img/logoJAVA2 - copia.png"
);
insert into lenguajes values(
"3", "Teams", "./img/logoteams.jpg"
);
insert into lenguajes values (
"4", "HTML", "./img/logoHTML - copia.png"
);

create table relacion (
idproyectos int,
idlenguajes int,
foreign key (idlenguajes) references relacion (idproyectos),
foreign key (idproyectos) references relacion (idlenguajes),
primary key(idproyectos, idlenguajes)
);

insert into relacion values (
"1", "1"
);
insert into relacion values (
"2", "2"
);

create table contacto (
id int primary key auto_increment,
nombre varchar(255),
apellidos varchar(255),
email varchar(255),
telefono varchar(255),
mensaje varchar(255)
);

create table footer(
id int primary key,
logo varchar(255),
telefono varchar(255),
gmail varchar(255)
);


insert into footer values(
"1", "./img/augmented-reality_327785.png", "609827591", "alejandroredomero@gmail.com"
);
